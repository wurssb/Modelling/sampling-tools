import pathlib

import time
import csv

import cobra
import pandas as pd

import sys
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, '..')
from sampling_tools import sampling, util

if __name__ == "__main__":
    # ----------------------------------------------------------------------
    # Loop throug parameters
    # ----------------------------------------------------------------------
    optimality_list = [0.5, 0.75, 0.9, 0.95, 0.99]
    flux_fraction_list = [1, 1.1, 1.25, 1.5]

    # Lists to name folders
    opt_namelist = [50, 75, 90, 95, 99]
    ff_namelist = [0, 10, 25, 50]

    with open('runtimes_yeast.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['optimality', 'flux_fraction', 'runtime'])  

        for optimality, o_name in zip(optimality_list, opt_namelist):
            for flux_fraction, ff_name in zip(flux_fraction_list, ff_namelist):
                
                start_time = time.time()

                # ----------------------------------------------------------------------
                # General settings
                # ----------------------------------------------------------------------
                model_directory = pathlib.Path("../example_data")
                cache_directory = pathlib.Path("../cache/yeast")

                results_directory = pathlib.Path("../results/yeast").joinpath(f"parameters_o{o_name}_ff{ff_name}")
                results_directory.mkdir(parents=True, exist_ok=True)
                model_path = pathlib.Path(model_directory / "Yeast8_NAR.xml").resolve()

                target_id = "nar_c"
                biomass_reaction_id = "r_4041"
                substrate_id = "s_0565[e]" #"s_0565__91__e__93__"
                substrate_exchange_id = "r_1714"
                maintenance_reaction_id = "r_4046"
                # Change the solver to "glpk" if you don't have a gurobi license.
                solver = "glpk" #"gurobi"

                # ----------------------------------------------------------------------
                # Loading model & medium.
                # ----------------------------------------------------------------------
                if cache_directory:
                    cache_directory.mkdir(exist_ok=True, parents=True)

                model = cobra.io.read_sbml_model(str(model_path))
                biomass_reaction = model.reactions.get_by_id(biomass_reaction_id)
                maintenance_reaction = model.reactions.get_by_id(maintenance_reaction_id)
                substrate = model.metabolites.get_by_id(substrate_id)
                substrate_exchange = model.reactions.get_by_id(substrate_exchange_id)

                model.solver = solver
                # max_tolerance = model.tolerance

                # Medium constraints
                inf = float("inf")
                # Substrate
                model.reactions.get_by_id(substrate_exchange_id).bounds = (-10, inf)
                # Oxygen
                model.reactions.get_by_id("r_1992").bounds = (-inf, inf)

                # Set the target export reaction.
                target_export = model.reactions.get_by_id("EX_NAR")

                # Constrain all reactions bounds to a maximum (1000).
                # No need to have truly infinite bounds when sampling.
                max_bound = sampling.COBRA_SOFT_INFINITY
                util.limit_bounds(model, max_bound)

                # Categorize reactions into different groups, which can be used to filter them out
                # from analysis. Because it takes a while, we save it to a cache.
                reaction_sets = util.get_cached_reaction_sets(
                    model, maintenance_reaction_id, biomass_reaction_id, cache_directory
                )

                # Explanation:
                #   required = ATPM + Biomass
                #   not biological = boundary / exchange / sinks / demands
                #   blocked = unable to carry flux
                #   no_genes = no annotated gene
                #   essential = essential reaction
                #   essential_by_gene = reaction that have an essential gene associated
                #                       but are not necessarily essential
                #   transport = reactions in multiple compartments
                all_reactions, required, not_biological, blocked = reaction_sets[:4]
                no_genes, essential, essential_by_gene, transport = reaction_sets[4:]
                ignore = required | not_biological | blocked | no_genes | transport | essential

                # ----------------------------------------------------------------------
                # Sampling and analysis
                # ----------------------------------------------------------------------
                sampling_settings = {
                    "biomass_reaction": biomass_reaction.id,
                    "target_export": target_export.id,
                    # Number of samples generated
                    "n_samples": 100000,
                    # Optimality required for the different cases
                    # i.e. production > 0.9 * max_production
                    "optimality": optimality,
                    # Number of samples thrown away for each sample generated
                    "thinning": 100, 
                    "processes": 4,
                    # A constraint is added for the total sum of flux to be less then
                    # x times the WT pFBA flux. This can partially avoid unrealistic fluxes
                    # with large loops etc. "objective fraction" is the pFBA optimality
                    # and flux fraction is a multiplier for the added constraint.
                    "objective_fraction": 1,
                    "flux_fraction": flux_fraction,
                    # Set any fluxes lower then the tolerance to 0.
                    "cutoff_tolerance": True,
                }

                valid_samples, convergence = sampling.sample(
                    model, sampling_settings, results_directory
                )

                # ----------------------------------------------------------------------
                # Record runtime
                # ----------------------------------------------------------------------
                end_time = time.time()
                runtime = end_time - start_time
                csv_writer.writerow([optimality, flux_fraction, runtime])

                # ----------------------------------------------------------------------
                # Save sampling results
                # ----------------------------------------------------------------------
                # Settings for significance and correlation
                significance_thresholds = {
                    "ks_cutoff_1": 0,
                    "ks_cutoff_2": 0,
                    "p_cutoff": 0.05,
                    "biomass_correlation_max": 0,
                    "abs_change_cutoff": 0.0001,
                }
                # Settings for clustering reactions into groups based on flux.
                clustering_settings = {
                    "condition": "producing",
                    "method": "average",
                    "metric": "euclidean",
                    "criterion": "distance",
                    "cut_threshhold": 0.25,
                }
                # Filtering of reactions
                (
                    results_per_reaction,
                    results_per_gene,
                    gene_order,
                    correlation,
                ) = sampling.summarize_results(
                    valid_samples,
                    model,
                    target_export.id,
                    biomass_reaction_id,
                    essential,
                    None,
                    significance_thresholds,
                    clustering_settings,
                )
                # ----------------------------------------------------------------------
                # Generate Excel file
                # ----------------------------------------------------------------------
                skip_output_columns = {}
                
                column_mapper = {
                    "mean (condition_1)": "growing (mean)",
                    "sd (condition_1)": "growing (sd)",
                    "mean (condition_3)": "producing (mean)",
                    "sd (condition_3)": "producing (sd)",
                }
                
                merge = True  # Merge cells in gene columns in excel sheet output.
                output, output_frame, output_order = "genes", results_per_gene, gene_order
                with pd.ExcelWriter(f"../results/yeast/parameters_o{o_name}_ff{ff_name}/sampling_results_opt{o_name}_ff{ff_name}.xlsx") as excelwriter:
                    # Remove/rename output columns
                    output_frame = output_frame.loc[
                        :, output_frame.columns.difference(skip_output_columns)
                    ].rename(columns=column_mapper)

                    # Knock-down/out targets.
                    if (output_order < 1).any():
                        output_frame.loc[output_order.index[output_order < 1]].to_excel(
                            excelwriter,
                            sheet_name="{} - knock-down".format(output),
                            merge_cells=merge,
                        )
                    # For overexpression, reverse the order.
                    if (output_order > 1).any():
                        output_frame.loc[output_order.index[output_order > 1][::-1]].to_excel(
                            excelwriter,
                            sheet_name="{} - over-express".format(output),
                            merge_cells=merge,
                        )

                    # Sort to get unassociated at the bottom
                    output_frame.loc[
                        output_order.index[output_order.isna()],
                    ].sort_index().to_excel(
                        excelwriter,
                        sheet_name="{} - insignificant".format(output),
                        merge_cells=merge,
                    )
                    # Tab with all genes
                    output_frame.to_excel(
                        excelwriter, sheet_name="{} - all".format(output), merge_cells=merge
                    )

                    # Complete info per reaction as well
                    results_per_reaction.to_excel(excelwriter, sheet_name="reactions - all")