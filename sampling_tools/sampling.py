#!/usr/bin/python3 

"""
Functions required to run Comparative Flux Sampling Analysis (CFSA)
"""

import contextlib
import hashlib
import json
import os
import pathlib
import warnings

import cobra
import numpy as np
import pandas as pd
import scipy
import scipy.cluster
import scipy.stats

from . import multilevelpool, util

COBRA_SOFT_INFINITY = 1000


def sample(model, sampling_settings, cache_dir=pathlib.Path("cache")):
    """Sample using optGP flux sampling.

    :param model: The Cobra model used for sampling, with constraints and objective set.
                  Can also be provided as a file path to a json cobra model.
    :type model: cobra.Model or path
    :param sampling_settings: Sampling setts
    :type sampling_settings: dict
    :param cache_dir: directory save the cached samples, defaults to pathlib.Path("cache").
                      Set to None for no caching.
    :type cache_dir: pathlib.Path, optional
    :return: (Samples, geweke convergence diagnostic)
    :rtype: (pd.DataFrame, pd.DataFrame)
    """
    # If the model is given as string, we try to load it using the json io.
    if isinstance(model, (str, pathlib.Path)):
        model = cobra.io.from_json(str(model))
    # See if we have the samples ready in the cache, and whether we should save them later.
    if cache_dir:
        hasher = hashlib.sha1()
        hasher.update(cobra.io.to_json(model, sort=True).encode("ascii"))
        # Drop the number of processes key since it doesn't change the result.
        # If we check the convergence on a per chain basis, the number of processes.
        # does matter, as it changes the individual chain length!
        # sampling_settings_copy = sampling_settings.copy()
        # sampling_settings_copy["processes"]
        hasher.update(json.dumps(sampling_settings, sort_keys=True).encode("ascii"))
        hash_str = hasher.hexdigest()
        cache_samples_path = cache_dir / (hash_str + "_samples.h5")
        cache_convergence_path = cache_dir / (hash_str + "_geweke.h5")
        if cache_samples_path.exists() and cache_convergence_path.exists():
            update_cache = False
            samples = convergence = None
            try:
                samples = pd.read_hdf(cache_samples_path)
                warnings.warn(
                    "Samples retrieved from cache: {}".format(str(cache_samples_path))
                )
            except Exception:
                warnings.warn(
                    "Could not load samples from cache: {}".format(
                        str(cache_samples_path)
                    )
                )
            try:
                convergence = pd.read_hdf(cache_convergence_path)
                print(
                    "Convergence scores retrieved from cache: {}".format(
                        str(cache_convergence_path)
                    )
                )
            except Exception:
                warnings.warn(
                    "Could not load convergence scores from cache: {}".format(
                        str(cache_convergence_path)
                    )
                )
            if samples is not None and convergence is not None:
                return samples, convergence
            else:
                update_cache = True
        else:
            warnings.warn(
                "Model and settings not found in cache: {}".format(
                    str(cache_samples_path)
                )
            )
            update_cache = True

    # Settings for sampler.
    # Number of samples generated (after thinning)
    n_samples = sampling_settings["n_samples"]
    # Optimality required for the different cases
    # i.e. production > 0.9 * max_production
    optimality = sampling_settings["optimality"]
    # Number of samples thrown away for each sample generated
    thinning = sampling_settings["thinning"]
    processes = sampling_settings["processes"]
    # A constraint is added for the total sum of flux to be less then
    # x times the WT pFBA flux. This can partially avoid unrealistic fluxes
    # with large loops etc. "objective fraction" is the pFBA optimality
    # and flux fraction is a multiplier for the added constraint.
    objective_fraction = sampling_settings["objective_fraction"]
    flux_fraction = sampling_settings["flux_fraction"]
    # Set any fluxes lower then the tolerance to 0?
    cutoff_tolerance = sampling_settings["cutoff_tolerance"]
    # Target for "growth" scenario
    biomass_reaction = model.reactions.get_by_id(sampling_settings["biomass_reaction"])
    # Target for "production" scenario
    target_export = model.reactions.get_by_id(sampling_settings["target_export"])

    # Check growth and production limits.
    b1, t1, b2, t2 = util.verify_target_production(
        model, biomass_reaction, target_export
    )

    # Check what growth would be still possible under the t2 * optimality constraint.
    # This will be the reference.
    with model:
        target_export.lower_bound = t2 * optimality
        model.objective = biomass_reaction
        slow_growth = model.optimize().objective_value

    # Runs we want to do.
    runs = [
        ("growing (pFBA)", biomass_reaction, "lower", b1 * optimality, True),
        ("growing (pFBA, slow)", biomass_reaction, "upper", slow_growth, True),
        ("producing", target_export, "lower", t2 * optimality, False),
    ]

    all_samples = []
    geweke_diagnostics = np.full((len(runs) * processes, len(model.reactions)), np.NaN)
    for run_index, (name, target, bound_direction, bound, add_pfba) in enumerate(runs):
        with model:
            if bound_direction == "upper":
                target.upper_bound = bound
            else:
                target.lower_bound = bound

            if add_pfba:
                util.add_pFBA_constraint(
                    model, biomass_reaction, objective_fraction, flux_fraction
                )
            # Redirect gurobipy output to /dev/null to avoid spamming stdout.
            with open(os.devnull, "w") as devnull:
                with contextlib.redirect_stdout(devnull):
                    sampler = cobra.sampling.OptGPSampler(
                        model=model, processes=processes, thinning=thinning
                    )
                    samples = sampler.sample(n_samples)

            # Remove invalid samples
            valid = sampler.validate(samples) == "v"
            samples[~valid] = np.NaN
            valid_samples = samples[valid]

            # The geweke diagnostic should be considered per chain and variable, so we have
            # to reconstruct the chains from the total samples. We can calculate the
            # chain length using the same method OptGPSampler used.
            # Note: this might be different in a different version of cobrapy!
            # chain_length = np.ceil(n_samples / processes).astype(int)
            # Alternatively, we can use the fact that cobrapy returns more samples to get to
            # an equal number per process and just split the samples by the number of processes.
            for chain_index, chains in enumerate(np.split(samples, processes)):
                geweke_diagnostics[
                    run_index * processes + chain_index, :
                ] = util.geweke(chains)
            # TODO: Check another criteria such as rhat? (Available in Arviz)

            all_samples.append(valid_samples.assign(condition=name))

    # Combine all data into one frame.
    all_samples = pd.concat(all_samples, ignore_index=True)
    geweke_diagnostics = pd.DataFrame(
        data=geweke_diagnostics,
        columns=pd.Index(samples.columns, name="reaction"),
        index=pd.MultiIndex.from_product(
            [[i[0] for i in runs], list(range(processes))], names=["condition", "chain"]
        ),
    )

    # Clip small values based on tolerances
    # (This makes it easier later to filter out true inactive reactions later,
    #  it also prevents the small values from generating real but invalid correlations)
    if cutoff_tolerance:
        condition = all_samples.condition
        all_samples = all_samples.drop("condition", axis=1)
        all_samples[
            (all_samples < model.tolerance) & (all_samples > -model.tolerance)
        ] = 0
        all_samples = all_samples.assign(condition=condition)

    # In principle you could round or trunctate all results based on the tolerance.
    # but in practice there should be any difference unless your model is opperating
    # dangerously close to the tolerance tresholds.
    # all_samples = all_samples.round(int(-np.log10(model.tolerance)))
    # all_samples = np.trunc((all_samples * model.tolerance )) * model.tolerance

    # Update cache if needed.
    if cache_dir and update_cache:
        all_samples.to_hdf(cache_samples_path, key="samples", complevel=6)
        geweke_diagnostics.to_hdf(
            cache_convergence_path, key="convergence", complevel=6
        )
    return all_samples, geweke_diagnostics


def apply_intervention(
    model, reaction, intervention, samples, bound_percentile, verbose=False
):
    """Update the cobra model to mimick the effect of an experimental intervention.

    :param model: Cobra model to modify
    :type model: cobra.Model
    :param reaction: Cobra reaction
    :type reaction: cobra.Reaction
    :param intervention: Type of intervention that should be applied.
    :type intervention: string ('over-expression', 'knock-down' or 'knock-out')
    :param samples: Dataframe with samples, used to calculate the value for the constraint.
    :type samples: pd.DataFrame
    :param bound_percentile: The percentile in the samples used for the new constraint.
    :type bound_percentile: float
    :param verbose: Print modifications, defaults to False
    :type verbose: bool, optional
    :raises NotImplementedError: Over-expression is not implemented for reversible fluxes.
    :raises ValueError: On invalid type of intervention.
    """
    # Since a change in expression doesn't modify the direction, but only the absolute
    # flux level, we also look at the absolute fluxes in the sample.
    abs_flux_limit = samples.abs().quantile(bound_percentile)
    # For overexpression, restrict to high flux level.
    if intervention == "over-expression":
        # If the reaction is only forward or backward we can keep it simple.
        if reaction.lower_bound == 0:
            reaction.lower_bound = abs_flux_limit
        elif reaction.upper_bound == 0:
            reaction.upper_bound = -abs_flux_limit
        else:
            raise NotImplementedError(
                "Over-expression intervention for reversible flux not functional."
            )
            # Since we want to restrict the absolute flux bounds,
            # create a helper variable and constraints.
            # TODO: This works for straightforward optimization, but not for sampling
            #       or FVA (constraints break after using of FVA...).
            #       Also it does not work if the model is not optimized before
            #       adding the constraints.
            #       Easier to split the reaction into fw + rev, but will require
            #       a change in post-processing of the sampling.
            # TODO: Does it impact the flux distribution to have two reactions
            #       that are subtracted?
            # TODO: First check if the constraints and proxy variable already exist and
            #       modify their bounds if they do instead.
            abs_proxy = model.problem.Variable(
                name="abs_proxy_{}".format(reaction.id),
                lb=10,
                ub=COBRA_SOFT_INFINITY,
                type="continuous",
            )
            c1 = model.problem.Constraint(
                expression=reaction.forward_variable
                - (abs_proxy - reaction.reverse_variable),
                name="abs_proxy_{}_c_pos".format(reaction.id),
                lb=0,
                ub=None,
            )
            c2 = model.problem.Constraint(
                expression=reaction.reverse_variable
                - (abs_proxy - reaction.forward_variable),
                name="abs_proxy_{}_c_neg".format(reaction.id),
                lb=0,
                ub=None,
            )
            model.add_cons_vars([abs_proxy, c1, c2])
        if verbose:
            print(
                "Target {} ({}) constrained to >= {}".format(
                    reaction, intervention, abs_flux_limit
                )
            )
    # For knock-down, restrict to low flux level.
    elif intervention == "knock-down":
        if reaction.reversibility:
            reaction.bounds = (-abs_flux_limit, abs_flux_limit)
        elif reaction.lower_bound == 0:
            reaction.lower_bound = -abs_flux_limit
        elif reaction.upper_bound == 0:
            reaction.upper_bound = abs_flux_limit
        if verbose:
            print(
                "Target {} ({}) constrained to <= {}".format(
                    reaction, intervention, abs_flux_limit
                )
            )
    # For knock-out, restrict to no flux.
    elif intervention == "knock-out":
        reaction.lower_bound, reaction.upper_bound = 0, 0
        if verbose:
            print(
                "Target {} ({}) constrained to == {}".format(reaction, intervention, 0)
            )
    else:
        raise ValueError("Invalid intervention")


def sample_pairs(
    model,
    target_df,
    samples,
    sampling_settings,
    bound_percentile=0.25,
    n_processes_per_sampler=None,
    n_concurrent_samplers=None,
    cache_dir=pathlib.Path("cache"),
    combined=False,
):
    """Sample possible interventions for significant flux differences between growth and production.

    :param model: Cobra model to sample from.
    :type model: cobra.Model
    :param target_df: Dataframe of targets with interventions
    :type target_df: pd.DataFrame
    :param samples: Samples in reference scenario - as returned from `sample`
    :type samples: pd.DataFrame
    :param sampling_settings: Sampling setttings - see `sample`
    :type sampling_settings: dict
    :param bound_percentile: Percentile of flux to use as a new bound,
                             defaults to 0.25 (see `apply intervention`)
    :type bound_percentile: float, optional
    :param n_processes_per_sampler: Number of process for each sampler, leave as None to set automatically
    :type n_processes_per_sampler: int, optional
    :param n_concurrent_samplers: Number of samplers running at the same time, leave as None to set automatically
    :type n_concurrent_samplers: int, optional
    :param cache_dir: Directory to save results for caching, defaults to pathlib.Path("cache")
    :type cache_dir: pathlib.Path, optional
    :param combined: Return all scenarios as one large dataframe? (defaults to False)
    :type combined: bool, optional
    :return: list of samples per scenario (or one large dataframe)
    :rtype: [pd.DataFrame]
    """
    # Since the preparation (warm-up) before sampling can start is rather long and
    # not parallelized, it is useful to run multiple samplings in parallel and not
    # to use many cores for each sampling as it would lead to large periods of
    # single core usage. As the number of samples increases, this becomes less
    # relevant.
    # ---
    # Check number of physical cores if possible, since hyper-threading will often be
    # slower for numerical workloads.
    try:
        import psutil
    except ImportError:
        available_cores = len(os.sched_getaffinity(0)) // 2
    else:
        available_cores = psutil.cpu_count(logical=False)
    # Always leave one core available for OS and process coordination.
    if n_processes_per_sampler is None and n_concurrent_samplers is None:
        # Between 1 to 4 cores per sampler
        n_processes_per_sampler = min(max(1, available_cores // 2), 4)
        # Fill up the available cores, rounding down.
        n_concurrent_samplers = available_cores // n_processes_per_sampler
    elif n_concurrent_samplers is None:
        n_concurrent_samplers = max(1, available_cores // n_processes_per_sampler)
    elif n_processes_per_sampler is None:
        n_processes_per_sampler = max(1, available_cores // n_concurrent_samplers)
    print(
        "Running {} parallel processes on {} cores each.".format(
            n_concurrent_samplers, n_processes_per_sampler
        )
    )
    sampling_settings["processes"] = n_processes_per_sampler

    # If we have one sampler only, run on a fake multiprocessing pool
    # using threads with less overhead and better debugging.
    original_model = model
    if n_concurrent_samplers == 1:
        import multiprocessing.dummy

        pool = multiprocessing.dummy.Pool
    else:
        pool = multilevelpool.MultiLevelPool

    with pool(processes=n_concurrent_samplers) as pool:
        target_names, results = [], []
        for group, targets in target_df.groupby("target_group"):
            # Copy the model and redirect gurobipy output to /dev/null
            # to avoid spamming stdout.
            with open(os.devnull, "w") as devnull:
                with contextlib.redirect_stdout(devnull):
                    model = original_model.copy()

            # Apply the intervention on all targeted reactions.
            for _, target in targets.iterrows():
                # Get correct samples for the reaction.
                production_samples = samples[samples.condition == "producing"][
                    target.reaction
                ]
                try:
                    apply_intervention(
                        model,
                        model.reactions.get_by_id(target.reaction),
                        target.intervention,
                        production_samples,
                        bound_percentile,
                    )
                except NotImplementedError:
                    warnings.warn(
                        "NotImplementedError for {} / {} ({})\nSkipping target".format(
                            target.group_name, target.reaction, target.intervention
                        )
                    )

            # Sample, filter and validate in the child process.
            target_names.append(targets.iloc[0].group_name)
            results.append(
                pool.apply_async(
                    sample, args=[cobra.io.to_json(model), sampling_settings, cache_dir]
                )
            )

        # Wait for pool to finish all work
        finished_results = []
        # TODO: Use convergence results
        for target, result in zip(target_names, results):
            try:
                samples, convergence = result.get(timeout=None)
                finished_results.append(samples.assign(target=target))
            except RuntimeError as e:
                warnings.warn("RunTimeError for {}: {}".format(target, e))
                finished_results.append(pd.DataFrame())

    # Save results
    # TODO: Save convergence as well!
    if combined:
        return pd.concat(finished_results, ignore_index=True)
    else:
        return finished_results


def score_reactions(
    model, samples, target_export_id, biomass_id, essential, gene_name=None
):
    """Score reactions based on several criteria.

    :param model: Cobra model to sample from.
    :type model: cobra.Model
    :param samples: Samples - as returned from `sample`
    :type samples: pd.DataFrame
    :param target_export_id: ID of the target reaction in the model.
    :type target_export_id: str
    :param biomass_id: ID of the biomass reaction in the model.
    :type biomass_id: str
    :param essential: List of IDs of essential reactions.
    :type essential: [str]
    :return: Dataframe with summarized results per reaction.
    :rtype: pd.DataFrame
    """
    # Check for differences in distributions.
    significance_results = []
    c1, c2, c3 = samples.condition.unique()
    for column in samples.columns:
        if column == "condition":
            continue
        c1_samples = samples[column][samples.condition == c1]
        c2_samples = samples[column][samples.condition == c2]
        c3_samples = samples[column][samples.condition == c3]
        # TODO: Check bug with 10.000 values and asymp (p=0) vs exact (p=1)
        # Not easily reproducible with random normal data...
        # use 'asymp' for now. It should be more or less valid anyway with
        # the number of samples we're using.
        ks1, pval1 = scipy.stats.ks_2samp(c1_samples, c2_samples, "two-sided", "asymp")
        ks2, pval2 = scipy.stats.ks_2samp(c1_samples, c3_samples, "two-sided", "asymp")
        ks3, pval3 = scipy.stats.ks_2samp(c2_samples, c3_samples, "two-sided", "asymp")
        significance_results.append(
            (
                column,
                c1,
                c1_samples.mean(),
                c1_samples.std(),
                c2,
                c2_samples.mean(),
                c2_samples.std(),
                c3,
                c3_samples.mean(),
                c3_samples.std(),
                ks1,
                pval1,
                ks2,
                pval2,
                ks3,
                pval3,
            )
        )
    results = pd.DataFrame(
        significance_results,
        columns=[
            "reaction",
            "condition_1",
            "mean (condition_1)",
            "sd (condition_1)",
            "condition_2",
            "mean (condition_2)",
            "sd (condition_2)",
            "condition_3",
            "mean (condition_3)",
            "sd (condition_3)",
            "ks-statistic (1v2)",
            "p-value (1v2)",
            "ks-statistic (1v3)",
            "p-value (1v3)",
            "ks-statistic (2v3)",
            "p-value (2v3)",
        ],
    ).set_index(["reaction"])

    # Do multiple testing correction
    # Not really useful since with 10k samples everything will be significant.
    # rejected, pval_corrected, alphacSidak, alphacBonf = statsmodels.stats.multitest.multipletests(
    #   results["p-value"], alpha=0.05, method="bonferroni"
    # )

    results = results.assign(
        **{
            # Add the reaction name for easier identification.
            "name": [model.reactions.get_by_id(r).name for r in results.index],
            # As well as the reaction equation.
            "reaction_equation": [
                model.reactions.get_by_id(r).build_reaction_string(True)
                for r in results.index
            ],
            # And the gene rule.
            "gene_rule": [
                model.reactions.get_by_id(r).gene_reaction_rule for r in results.index
            ],
            # Absolute fold change of flux means.
            # We use the absolute values here and in the next criteria, since we
            # assume that the gene expression somehow correlates with the absolute flux
            # irrespective of the direction of the reaction in the model.
            "mean_fold_change": np.abs(
                results["mean (condition_3)"] / results["mean (condition_1)"]
            ),
            # Mean relative change (irrespective of direction)
            "mean_rel_change": np.abs(
                np.abs(
                    np.abs(results["mean (condition_3)"])
                    - np.abs(results["mean (condition_1)"])
                )
                / (
                    results[["mean (condition_3)", "mean (condition_1)"]]
                    .abs()
                    .max(axis=1)
                )
            ),
            # Mean absolute change can be used to filter out significant but low fluxes.
            "mean_absolute_change": np.abs(
                np.abs(
                    np.abs(results["mean (condition_3)"])
                    - np.abs(results["mean (condition_1)"])
                )
            ),
            # Is the reaction essential for the production of biomass?
            "essential": results.index.isin(essential),
            # How strongly does the flux correlate with the production target flux?
            "target_correlation": (
                samples[samples.condition == "producing"]
                .drop("condition", axis=1)
                .corrwith(samples[samples.condition == "producing"][target_export_id])
            ),
            # How strongly does the flux correlate with the biomass flux?
            "biomass_correlation": (
                samples[samples.condition == "producing"]
                .drop("condition", axis=1)
                .corrwith(samples[samples.condition == "producing"][biomass_id])
            ),
        }
    )
    # If we have gene name replacements, replace them here.
    if gene_name is not None:
        results = results.assign(
            gene_names=results.gene_rule.replace(gene_name, regex=False)
        )
    else:
        results = results.assign(gene_names=results.gene_rule)

    return results


def get_significant_reactions(results, significance_thresholds):
    """Select significant reactions based on thresholds.

    :param results: Dataframe with summarized results per reaction
                    (as returned by score_reactions).
    :type results: pd.DataFrame
    :param significance_thresholds: Dictionairy with thesholds for assigning reactions as significant.
                    Keys:
                        ks_cutoff_1 - Cutoff for distribution overlap between growth & production
                        ks_cutoff_2 - Cutoff for distribution overlap between growth and the slow
                            growth control scenario. This is usefull to filter out fluxes that
                                are low / high  just because there is low growth in the production
                                case. i.e. transport reactions for minerals
                        p_cutoff - p_cutoff will be scaled by dividing by the number of results.
                            i.e. correct for multiple testing using the Bonferroni method.
                        biomass_correlation_max - Maximal correlation that a flux can have with
                            the biomass. Can be used to filter out even more reactions that are
                            solely linked to high or low growth and unrelated to production.
                        abs_change_cutoff - This is the minimal level of change in flux we choose
                            for a reaction that is feasible for intervention. One could argue
                            that this should be lower, since some reactions might have extremely
                            low fluxes even when fully utilized, such as the production of a large
                            cofactor complex. On the other hand, these reactions are likely not
                            good targets either.
    :type significance_thresholds: {str: float}
    :return: Dataframe with summarized results for significant reactions.
    :rtype: pd.DataFrame
    """
    ks_cutoff_1 = significance_thresholds["ks_cutoff_1"]
    ks_cutoff_2 = significance_thresholds["ks_cutoff_2"]
    # Apply bonferoni correction for multiple testing.
    p_cutoff = significance_thresholds["p_cutoff"] / len(results)
    biomass_correlation_max = significance_thresholds["biomass_correlation_max"]
    abs_change_cutoff = significance_thresholds["abs_change_cutoff"]

    # Marked as significant only if *all* criteria are met.
    significant = results[
        (results["ks-statistic (1v3)"] >= ks_cutoff_1)
        & (results["ks-statistic (2v3)"] >= ks_cutoff_2)
        & (results["p-value (1v3)"] <= p_cutoff)
        & (results["p-value (2v3)"] <= p_cutoff)
        # If there is no or constant biomass production, correlation will be NaN, so
        # we can ignore this criteria by setting the correlation to 0 for this scenario.
        & (results["biomass_correlation"].abs().fillna(0) <= biomass_correlation_max)
        & (results["mean_absolute_change"] >= abs_change_cutoff)
        & (results["gene_rule"] != "")
    ].sort_values("mean_fold_change")

    # Classify as over-expression or know-down.
    # Non-essential knock-down genes can be further marked as possible knock-out targets.
    intervention = np.empty(len(significant), dtype="U15")
    intervention[significant.mean_fold_change > 1] = "over-expression"
    intervention[significant.mean_fold_change < 1] = "knock-down"
    intervention[
        (significant.mean_fold_change < 1) & ~(significant.essential)
    ] = "knock-out"
    notes = np.empty(len(significant), dtype="U100")
    # We also note if there might be multiple iso-enzymes, since these reactions
    # might require multiple interventions to be effective.
    notes[significant.gene_rule.str.contains("or")] = "Iso-enzymes"

    significant = significant.assign(intervention=intervention, notes=notes)
    return significant


def get_correlation_groups(
    samples,
    condition,
    significant=None,
    abs_correlation=True,
    method="average",
    metric="euclidean",
    criterion="distance",
    cut_threshhold=0.25,
):
    """Cluster reactions into groups based on correlation of the (absolute) fluxes between samples.

    Reactions that are constant for all samples are dropped.

    :param samples: Samples - as returned from `sample`
    :type samples: pd.DataFrame
    :param condition: condition where the correlation groups will be found
    :type condition: string ('growing' or 'producing')
    For details on other arguments and outputs see scipy.cluster.hierarchy.linkage and
    scipy.cluster.hierarchy.fcluster
    """
    # Take the right condition
    df = samples.loc[samples.condition == condition]
    # Drop insignificant (if wanted)
    if significant is not None:
        df = df[significant.index]
    # Drop all constant rows.
    df = df.loc[:, (df != df.iloc[0, :]).any()]

    # Calculate correlations.
    correlation = df.corr()
    if abs_correlation:
        correlation = np.abs(correlation)

    # Identify correlation groups using clustering.
    linkage_matrix = scipy.cluster.hierarchy.linkage(
        correlation, method=method, metric=metric
    )
    groups = pd.Series(
        scipy.cluster.hierarchy.fcluster(
            linkage_matrix, cut_threshhold, criterion=criterion
        ),
        index=correlation.index,
    )
    return groups, correlation, linkage_matrix


def summarize_results(
    samples,
    model,
    target_export_id,
    biomass_reaction_id,
    essential,
    gene_name,
    significance_thresholds,
    clustering_settings=None,
):
    """Create a summary, filtering out insignificant results and clustering into similar groups.
    :param samples: Samples - as returned from `sample`
    :type samples: pd.DataFrame
    :param model: The Cobra model used for sampling
    :type model: cobra.Model or path
    :param target_export_id: Cobra reaction target ID
    :type target_export_id: string
    :param biomass_reaction_id: Cobra reaction biomass ID
    :type biomass_reaction_id: string
    :param essential: List of IDs of essential reactions.
    :type essential: [str]
    :param gene_name: list of gene names or None
    :type gene_name: [str]
    :param significance_thresholds: Dictionairy with thesholds for assigning reactions as significant.
                    Keys:
                        ks_cutoff_1 - Cutoff for distribution overlap between growth & production
                        ks_cutoff_2 - Cutoff for distribution overlap between growth and the slow
                            growth control scenario. This is usefull to filter out fluxes that
                                are low / high  just because there is low growth in the production
                                case. i.e. transport reactions for minerals
                        p_cutoff - p_cutoff will be scaled by dividing by the number of results.
                            i.e. correct for multiple testing using the Bonferroni method.
                        biomass_correlation_max - Maximal correlation that a flux can have with
                            the biomass. Can be used to filter out even more reactions that are
                            solely linked to high or low growth and unrelated to production.
                        abs_change_cutoff - This is the minimal level of change in flux we choose
                            for a reaction that is feasible for intervention. One could argue
                            that this should be lower, since some reactions might have extremely
                            low fluxes even when fully utilized, such as the production of a large
                            cofactor complex. On the other hand, these reactions are likely not
                            good targets either.
    :type significance_thresholds: {str: float}
    :param clustering_settings: settings
    :type clustering_settings: {str:float}
    :return df: 
    :rtype df:
    :return output_per_gene: 
    :rtype output_per_gene:
    :return order: 
    :rtype order:
    :return correlation: 
    :rtype correlation:
    """
    if clustering_settings is None:
        clustering_settings = {"condition": "producing"}

    nonzero_samples = samples.loc[:, ~(samples == 0).all(axis=0)]

    results = score_reactions(
        model,
        nonzero_samples,
        target_export_id,
        biomass_reaction_id,
        essential,
        gene_name,
    )

    significant = get_significant_reactions(results, significance_thresholds)
    df = results.assign(
        significant=results.index.isin(significant.index),
        intervention=significant.intervention,
    )
    df.intervention = df.intervention.fillna("")

    groups, correlation, linkage_matrix = get_correlation_groups(
        nonzero_samples, **clustering_settings
    )

    # Assign correlation groups & iso-enzyme possibilities
    df = df.assign(
        correlation_group=groups, iso_enzyme=df.gene_rule.str.contains(" or ")
    )
    df.correlation_group = df.correlation_group.fillna(0).astype(int)

    # Organize per gene
    genes_per_reaction = get_all_genes(df.gene_names)
    results_output = duplicate_per_gene(df, genes_per_reaction)
    # Mark reactions with no genes as ~unassociated for clarity.
    results_output.gene = results_output.gene.replace("", "~unassociated")

    # Determine order (ignoring insignificant entries)
    output_per_gene, order = sort_results(
        results_output, by="gene", column="mean_fold_change", ignore_insignificant=True
    )
    # order = (
    #     results_output.loc[results_output.intervention != ""]
    #     .groupby("gene")
    #     .mean_fold_change.mean()
    #     .reindex(results_output.gene.unique())
    #     .sort_values()
    # )
    # output_per_gene = results_output.set_index(["gene", "reaction"]).loc[order.index]
    return df, output_per_gene, order, correlation


def duplicate_per_gene(df, genes_per_reaction):
    """Duplicate a df so that every (gene, reaction) combination has its own line."""
    return (
        df.loc[genes_per_reaction.reaction]
        .assign(gene=genes_per_reaction.gene.values)
        .reset_index()
    )


def get_all_genes(column):
    """Get all genes from the GPR rule for each reaction in a dataframe."""
    return (
        column.str.replace(r"[\(\)]", "", regex=True)
        .str.split(r" and | or ")
        .apply(pd.Series, 1)
        .stack()
        .reset_index()
        .drop("level_1", axis=1)
        .set_axis(["reaction", "gene"], axis=1)
        # Sometimes genes occur multiple times in the same formula
        .drop_duplicates()
    )


def sort_results(df, by="gene", column="mean_fold_change", ignore_insignificant=True):
    """Sort the results dataframe.

    :param by: Variable to group by (gene, cluster, pathway), defaults to 'gene'
    :type by: str, optional
    :param column: Variable to sort groups by, defaults to "mean_fold_change"
    :type column: str, optional
    :param ignore_insignificant: Ignore entries not annotated with an intervention, defaults to True.
    :type ignore_insignificant: bool, optional
    """
    if ignore_insignificant:
        sort_df = df.loc[(df.intervention != "") | df.intervention.isna()]
    else:
        sort_df = df

    order = sort_df.groupby(by)[column].mean().reindex(df[by].unique()).sort_values()
    return df.set_index([by, "reaction"]).loc[order.index], order

