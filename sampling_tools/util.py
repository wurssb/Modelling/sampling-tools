#!/usr/bin/python3 

"""Utility functions for flux Comparative Flux Sampling Analysis (CFSA)."""

import hashlib
import itertools
import json
import warnings

import cobra
import numpy as np
import pandas as pd


def geweke(chain, beginning_fraction=0.1, end_fraction=0.5):
    """Calculate the Geweke diagnostic for chain convergence.

    Accepts a single 1-dimensional chain, or a 2-dimensional array of chains,
    with samples as rows and chains as columns.

    Note: Ignores NaN values.

    :param chain: One or more markov chains.
    :type chain: np.array like object (np.array, pd.Series, pd.Dataframe)
    :param beginning_fraction: The beginning fraction of the chain, defaults to 0.1
    :type beginning_fraction: float, optional
    :param end_fraction: The end fraction of the chain, defaults to 0.5
    :type end_fraction: float, optional
    :raises ValueError: On invalid fractions or non 1-dimensional chain.
    """
    if (
        beginning_fraction + end_fraction >= 1
        or min(beginning_fraction, end_fraction) <= 0
    ):
        raise ValueError("Invalid fraction")

    # Split the chain.
    if chain.ndim == 1:
        chain = np.atleast_2d(chain).T
    length = chain.shape[0]

    # First part.
    end_of_beginning = int(length * beginning_fraction)
    start = chain[:end_of_beginning]

    start_of_end = length - int(length * end_fraction)
    end = chain[start_of_end:]

    # Calculate the diagnostic. Silence numpy errors caused by divide by zero.
    with np.errstate(divide="ignore", invalid="ignore"):
        geweke_diagnostic = (
            np.nanmean(start, axis=0) - np.nanmean(end, axis=0)
        ) / np.sqrt(np.nanvar(start, axis=0) + np.nanvar(end, axis=0))
    # Replace +/-infinity from divide by zeros by NaN values.
    geweke_diagnostic[np.isinf(geweke_diagnostic)] = np.NaN

    # Return in less dimension as input.
    return geweke_diagnostic if geweke_diagnostic.size > 1 else geweke_diagnostic[0]


def add_pFBA_constraint(model, objective, objective_fraction=1, flux_fraction=1):
    """Add a pFBA flux minimality constraint to the model. Updates the model in-place.

    :param model: The cobra model
    :type model: cobra.Model
    :param objective: The objective that should be maximized,
    in order to determine the objective bound for the flux sum minimization.
    :type objective: cobra.Reaction
    :param objective_fraction: pFBA minimum objective fraction, should be 1<= and defaults to 1.
    :type objective_fraction: float, optional
    :param flux_fraction: pFBA maximum flux fraction, should be >=1 and defaults to 1.
    :type flux_fraction: float, optional
    :return: The model with the flux minimality constraint added.
    :rtype: cobra.Model
    """

    def recursive_sum(x):
        """Recursive version of sum.

        Sum operations are slow on variables. By doing it recursively, we can use
        2log(length(x)) additions instead of length(x).
        We could exceed the recursion depth, but it is not very likely with normal model sizes.
        """
        length = len(x)
        if length == 1:
            return x[0]
        elif length == 2:
            return x[0] + x[1]
        else:
            split = length // 2
            return recursive_sum(x[split:]) + recursive_sum(x[:split])

    # Calculate flux sum for the objective.
    flux_sum_value = cobra.flux_analysis.pfba(model, objective_fraction, objective)

    # Create a total flux variable (fw + rev fluxes, same as pFBA does it in Cobra.)
    variables = ((r.forward_variable, r.reverse_variable) for r in model.reactions)
    sum_variable = recursive_sum(list(itertools.chain.from_iterable(variables)))
    # Create and add the constraint to the model.
    constraint = model.problem.Constraint(
        expression=sum_variable,
        name="total_flux_sum_bound",
        lb=0,
        ub=flux_sum_value.objective_value * flux_fraction,
    )
    model.add_cons_vars([constraint])
    return model





def verify_target_production(model, biomass_reaction, target_export, verbose=True):
    """Verify the model can produce the target and can grow.
    :param model: The cobra model
    :type model: cobra.Model
    :param biomass_reaction: biomass reaction ID
    :type biomass_reaction:str
    :param target_export: target reaction ID
    :type target_export: str
    :return: max biomass growth rate, max production under max growth 
    conditions, max production rate, max growth under max prod conditions
    :rtype:float
    """
    with model:
        model.objective_direction = "max"
        model.objective = biomass_reaction
        b1 = model.slim_optimize()
        assert b1 > 0
        biomass_reaction.lower_bound = b1
        model.objective = target_export
        t1 = model.slim_optimize()
        if verbose:
            print(
                "Native maximum biomass production: {:.2f}, allowing for "
                "{:.3f} production.".format(b1, t1)
            )
    with model:
        model.objective = target_export
        t2 = model.slim_optimize()
        assert t2 > 0
        target_export.lower_bound = t2
        model.objective = biomass_reaction
        b2 = model.slim_optimize()
        if verbose:
            print(
                "Native maximum production: {:.2f}, allowing for "
                "{:.3f} production of biomass.".format(t2, b2)
            )
    return b1, t1, b2, t2


def get_reaction_sets(model, maintenance, biomass, ids=False, processes=None):
    """Get reactions grouped into different categories.

    Explanation:
      required = ATPM + Biomass
      not biological = boundary / exchange / sinks / demands
      blocked = unable to carry flux
      no_genes = no annotated gene
      essential = essential reaction
      essential_by_gene = reaction that have an essential gene associated
                          but are not necessarily essential
      transport = reactions in multiple compartments

    """
    # All reactions as a set
    all_reactions = set(model.reactions)
    # Necessary reactions for the model to optimize properly
    required = {model.reactions.get_by_id(i) for i in (maintenance, biomass)}
    # All reactions that are not biological can be ignored.
    not_biological = (
        set(model.boundary)
        | set(model.exchanges)
        | set(model.sinks)
        | set(model.demands)
    )
    # Blocked reactions that can't carry flux.
    blocked = {
        model.reactions.get_by_id(i)
        for i in cobra.flux_analysis.find_blocked_reactions(model, processes)
    }
    # Reactions without genes.
    no_genes = {r for r in model.reactions if not r.genes}
    # essential reactions
    essential = cobra.flux_analysis.find_essential_reactions(model, processes)
    # reactions from essential genes
    essential_genes = cobra.flux_analysis.find_essential_genes(model, processes)
    essential_reactions_from_genes = set().union(
        *(g.reactions for g in essential_genes)
    )
    # transport reactions
    transport = {
        r for r in model.reactions if len({i.compartment for i in r.metabolites}) > 1
    }
    results = (
        all_reactions,
        required,
        not_biological,
        blocked,
        no_genes,
        essential,
        essential_reactions_from_genes,
        transport,
    )
    if ids:
        return tuple({r.id for r in l} for l in results)
    else:
        return results


def get_cached_reaction_sets(
    model, maintenance_reaction_id, biomass_reaction_id, cache_directory
):
    """See get_reaction_sets, with optional cache location."""
    if cache_directory:
        # Get filename with unique has for model.
        reaction_sets_file = (
            cache_directory
            / "reaction_sets"
            / "{}.json".format(
                hashlib.sha1(
                    cobra.io.to_json(model, sort=True).encode("ascii")
                ).hexdigest()
            )
        )
        # Check if it exists
        if reaction_sets_file.exists():
            warnings.warn(
                "Retrieved cached reaction sets from {}".format(str(reaction_sets_file))
            )
            # If so, extract sets.
            reaction_sets = tuple(
                set(i) for i in json.loads(reaction_sets_file.read_text())
            )
        # Else, create file and save.
        else:
            reaction_sets = get_reaction_sets(
                model, maintenance_reaction_id, biomass_reaction_id, ids=True
            )
            reaction_sets_file.parent.mkdir(exist_ok=True)
            reaction_sets_file.write_text(
                json.dumps([list(i) for i in reaction_sets], sort_keys=True, indent=2)
            )
    # If no cache is set, always create from scratch.
    else:
        reaction_sets = get_reaction_sets(
            model, maintenance_reaction_id, biomass_reaction_id, ids=True
        )
    return reaction_sets


def gene_to_fbc_gene(gene):
    """Convert gene in the form of PP{number}+ to gp_PP_(__{number}__)+."""
    numbers = {
        "0": "ZERO",
        "1": "ONE",
        "2": "TWO",
        "3": "THREE",
        "4": "FOUR",
        "5": "FIVE",
        "6": "SIX",
        "7": "SEVEN",
        "8": "EIGHT",
        "9": "NINE",
    }
    return "".join(["gp_PP_"] + ["__{}__".format(numbers[c]) for c in gene[-4:]])


def fbc_gene_to_gene(gene):
    """Convert fbc gene in the form of gp_PP_(__{number}__)+ to PP{number}+."""
    numbers = {
        "ZERO": "0",
        "ONE": "1",
        "TWO": "2",
        "THREE": "3",
        "FOUR": "4",
        "FIVE": "5",
        "SIX": "6",
        "SEVEN": "7",
        "EIGHT": "8",
        "NINE": "9",
    }
    return "".join(["PP"] + [numbers[i] for i in gene.strip("gpP_").split("____")])


def limit_bounds(model, limit):
    """Limit the bounds to a maximum flux. Operates in-place on the model.

    :param model: Cobra model
    :type model: cobra.Model
    :param limit: Maximum of absolute flux
    :type limit: int | float
    """
    for reaction in model.reactions:
        min_, max_ = reaction.bounds
        reaction.bounds = (max(min_, -limit), min(max_, limit))


