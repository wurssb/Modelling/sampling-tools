"""Extension of the cobrapy flux sampling functionality.

Includes extras such as:
    - Comparison of different scenarios
    - Filtering
    - Parallel execution of multiple scenarios
    - Caching
    - Convergence testing

Author: Rik van Rosmalen
"""
import setuptools


def readme():
    """Read readme from README.md."""
    with open("README.md", "r") as fh:
        return fh.read()


setuptools.setup(
    name="sampling_tools",
    version="0.0.1",
    author="Rik van Rosmalen",
    author_email="rikpetervanrosmalen@gmail.com",
    description="Extension of cobrapy for flux sampling analysis",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wurssb/Modelling/sampling-tools",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
    ],
    python_requires=">=3.7",
    install_requires=[
        "cobra",
        "numpy",
        "pandas>=1",
        "scipy",
        "h5py",
        "openpyxl",
        # "tables",
    ],
)
