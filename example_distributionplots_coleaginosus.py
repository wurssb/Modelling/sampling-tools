import pathlib

import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import numpy as np
import pandas as pd
import seaborn as sns

from sampling_tools import sampling, util

def load_results_tables(path):

    sheet_names = [
        "Down-regulations",
        "Over-expressions",
    ]
    results = {}
    with pd.ExcelFile(path) as file:
        for sheet, key in zip(sheet_names, ["KD", "OV"]):
            if sheet not in file.sheet_names:
                results.append(pd.DataFrame())
                continue
            df = pd.read_excel(file, sheet)
            if "genes" in sheet:
                df.gene = df.gene.ffill()
            # When reading empty strings, pandas makes na values.
            # Replace these with empty strings instead for easier comparison later.
            for column in df.columns:
                if df[column].dtype == "object":
                    df.loc[:, column] = df.loc[:, column].fillna("")
            results[key]=df
    return results
    




# Load CFSA results
results_path = pathlib.Path("results/crypto")
results = load_results_tables(results_path / "filtered_results.xlsx")

# Load sampling results
cache_path = next(pathlib.Path("cache/crypto").glob("*_samples.h5"))
samples = pd.read_hdf(cache_path) 

# Create a pdf file with distribution graphs for each intervention
for intervention in ["KD", "OV"]:
  print(f"Generating {intervention} plots...")
  pdf = matplotlib.backends.backend_pdf.PdfPages(results_path / f"{intervention}.pdf")
  data = results[intervention]
  for reaction, reaction_name in zip(list(data['reaction']), list(data['name'])):
    # Get samples of each condition
    s_reaction = samples[[reaction, 'condition']]
    s_growing = s_reaction[s_reaction['condition']=='growing (pFBA)']
    s_slow = s_reaction[s_reaction['condition']=='growing (pFBA, slow)']
    s_producing = s_reaction[s_reaction['condition']=='producing']
    # Calculate mean relative change between growth and production
    mean_rel_change = np.abs(
        np.abs(
            np.abs(s_producing[reaction].mean())
            - np.abs(s_growing[reaction].mean())
        )
        / (
            abs(
                max(
                    [s_producing[reaction].mean(),
                     s_growing[reaction].mean()]
                )
            )

        )
    )
    # Density plot
    fig = plt.figure(figsize=(12,8))
    sns.kdeplot(data=s_growing[reaction], fill=True)
    sns.kdeplot(data=s_slow[reaction], fill=True)
    sns.kdeplot(data=s_producing[reaction], fill=True)
    plt.title(f'{reaction}: {reaction_name} (mean_rel_change = {mean_rel_change:.5f})') 
    plt.legend(['growing (pFBA)', 'growing (pFBA, slow)', 'producing'])
    plt.close(fig)
    # Save in pdf
    pdf.savefig(fig)
  pdf.close()
  print(f"{intervention} plots generated")