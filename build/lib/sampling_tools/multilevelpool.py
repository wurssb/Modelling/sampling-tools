"""Multi-processing pool that allows the creathing of sub processes.

Because a python process from a multiprocessing pool is launched as a daemon
and therefore not able to launch it's own child processes, we can't use a double
parallel layer for the sampling of different targets.

Therefore a custom multiprocessing pool is used, from the following StackOverflow post:
https://stackoverflow.com/a/53180921
by the user "Massimiliano" (https://stackoverflow.com/users/771663/massimiliano)
under the CC BY-SA 4.0 license (https://creativecommons.org/licenses/by-sa/4.0/)
Applicable to the following classes:
- NoDeamonProcess
- NoDeamonContext
- MultiLevelPool

Note the caveat mentioned as well:
 "As for allowing children threads to spawn off children of its own using
  subprocess runs the risk of creating a little army of zombie 'grandchildren'
  if either the parent or child threads terminate before the subprocess
  completes and returns."
In practice this means that aborting a run manually can cause zombie-processes.
"""
import multiprocessing
import multiprocessing.pool


class NoDaemonProcess(multiprocessing.Process):
    @property
    def daemon(self):
        return False

    @daemon.setter
    def daemon(self, value):
        pass


class NoDaemonContext(type(multiprocessing.get_context())):
    Process = NoDaemonProcess


class MultiLevelPool(multiprocessing.pool.Pool):
    # We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool
    # because the latter is only a wrapper function, not a proper class.
    def __init__(self, *args, **kwargs):
        kwargs["context"] = NoDaemonContext()
        super(MultiLevelPool, self).__init__(*args, **kwargs)
