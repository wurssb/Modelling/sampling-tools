#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cobra
import cobra.test
from cobra import Model, Reaction, Metabolite
import os
from os.path import join

# Load model and data
data_dir = './'
model = cobra.io.read_sbml_model(join(data_dir, "Yeast8_NAR.xml"))
model.solver = 'glpk'
data = pd.read_excel(
    './single_results_groups.xlsx',
    sheet_name='groups - knock-down' #in this case reactions to knock down
)


# Select first 100 groups
top = 0
index = 0
for group in data['correlation_group']:
    if top < 100:
        index += 1
        if group >= 0:
            top+=1
    else:
        if group >= 0:
            break
        else:
            index += 1
top10 = data[0:index]
top10.loc[:,'correlation_group']=top10.loc[:,'correlation_group'].fillna(method="pad")

# Get metabolic groups associated which each reaction
df = top10[['correlation_group', 'reaction']].copy()
df['metabolic_group'] = df.apply(
    lambda row: model.get_associated_groups(model.reactions.get_by_id(row.reaction)), 
    axis = 1
)
# Some reactions are associated to various groups, get one row per group
df = df.explode('metabolic_group') # One row per value in the list of metabolic rows

# New colum with the name of the metabolic group
def conversion(value):
    try: 
        return value.name
    except:
        return np.nan
df['metabolic_group_id'] = df.apply(
    lambda row: conversion(row.metabolic_group), 
    axis = 1
)

# Transfrom df for plotting and plotting
df_plot = df.groupby(
    ['correlation_group', 'metabolic_group_id']).size().reset_index().pivot(
    columns='correlation_group', index='metabolic_group_id', values=0)
df_plot.plot(kind='bar', stacked=True, colormap='viridis')
plt.title("Reactions from top 100 groups")
plt.legend(ncol=5)





