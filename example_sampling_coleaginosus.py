#!/usr/bin/python3 

"""Example of application of CFSA to improve lipid production by C. oleaginosus"""

import pathlib
import cobra
import pandas as pd
from sampling_tools import sampling, util

if __name__ == "__main__":
    # cobra_config = cobra.Configuration()
    # cobra_config.processes = 1

    # ----------------------------------------------------------------------
    # General settings --> to be updated by user
    # ----------------------------------------------------------------------
    # Set directories
    model_directory = pathlib.Path("GEMs")
    cache_directory = pathlib.Path("cache/crypto")
    results_directory = pathlib.Path("results/crypto")
    results_directory.mkdir(parents=True, exist_ok=True)
    model_path = pathlib.Path(
        model_directory / "iNP636_Coleaginosus_ATCC20509_mod_names_V2.xml"
    ).resolve()

    # Set model settings
    target_id = "lipid_c"
    biomass_reaction_id = "Biomass_nitrogen_depletion"
    substrate_id = "glyc_e"
    nitrogen_id = "urea_c"
    substrate_exchange_id = "r_71_exchange"
    nitrogen_exchange_id = "r_160_exchange"
    maintenance_reaction_id = "ATPM"
    oxygen_exchange_id = "r_128_exchange"
    target_exchange_id = "lipid_synthesis"
    solver = "glpk" #"gurobi" # Change the solver to "glpk" if you don't have a gurobi license.

    # Sampling settings
    # Number of samples generated
    n_samples = 10_000
    # Number of samples thrown away for each sample generated
    thinning = 100
    processes = 4
    # Optimality required for the different cases
    # i.e. production > 0.9 * max_production
    optimality = 0.9
    # A constraint is added for the total sum of flux to be less then
    # x times the WT pFBA flux. This can partially avoid unrealistic fluxes
    # with large loops etc. flux fraction is a multiplier for the added constraint.
    flux_fraction = 1.25

    # ----------------------------------------------------------------------
    # Loading model & medium.
    # ----------------------------------------------------------------------
    if cache_directory:
        cache_directory.mkdir(exist_ok=True, parents=True)

    model = cobra.io.read_sbml_model(str(model_path))
    biomass_reaction = model.reactions.get_by_id(biomass_reaction_id)
    maintenance_reaction = model.reactions.get_by_id(maintenance_reaction_id)
    substrate = model.metabolites.get_by_id(substrate_id)
    substrate_exchange = model.reactions.get_by_id(substrate_exchange_id)
    nitrogen = model.metabolites.get_by_id(nitrogen_id)
    nitrogen_exchange = model.reactions.get_by_id(nitrogen_exchange_id)

    model.solver = solver
    max_tolerance = model.tolerance

    # Medium constraints
    inf = float("inf")
    model.reactions.get_by_id(substrate_exchange_id).bounds = (-20, inf)
    model.reactions.get_by_id(nitrogen_exchange_id).bounds = (-0.2, inf)
    model.reactions.get_by_id(oxygen_exchange_id).bounds = (-inf, inf)

    model.objetive = biomass_reaction
    solution = model.optimize()
    max_growth = solution.objective_value
    print("Max growth", max_growth)

    # Additionally, set a minimum of 10% growth.
    model.reactions.get_by_id("Biomass_nitrogen_depletion").bounds = (
        0.1 * max_growth,
        inf,
    )

    # Set the target export reaction.
    target_export = model.reactions.get_by_id(target_exchange_id)

    # Constrain all reactions bounds to a maximum (1000).
    # No need to have truly infinite bounds when sampling.
    max_bound = sampling.COBRA_SOFT_INFINITY
    util.limit_bounds(model, max_bound)

    # Categorize reactions into different groups, which can be used to filter them out
    # from analysis. Because it takes a while, we save it to a cache.
    reaction_sets = util.get_cached_reaction_sets(
        model, maintenance_reaction_id, biomass_reaction_id, cache_directory
    )

    # Explanation:
    #   required = ATPM + Biomass
    #   not biological = boundary / exchange / sinks / demands
    #   blocked = unable to carry flux
    #   no_genes = no annotated gene
    #   essential = essential reaction
    #   essential_by_gene = reaction that have an essential gene associated
    #                       but are not necessarily essential
    #   transport = reactions in multiple compartments
    all_reactions, required, not_biological, blocked = reaction_sets[:4]
    no_genes, essential, essential_by_gene, transport = reaction_sets[4:]
    ignore = required | not_biological | blocked | no_genes | transport | essential

    # ----------------------------------------------------------------------
    # Sampling and analysis
    # ----------------------------------------------------------------------
    # Set here the desires optimalitty and flux_fraction parameters
    sampling_settings = {
        "biomass_reaction": biomass_reaction.id,
        "target_export": target_export.id,
        # Number of samples generated
        "n_samples": n_samples,
        # Optimality required for the different cases
        # i.e. production > 0.9 * max_production
        "optimality": optimality,
        # Number of samples thrown away for each sample generated
        "thinning": thinning,
        "processes": processes,
        # A constraint is added for the total sum of flux to be less then
        # x times the WT pFBA flux. This can partially avoid unrealistic fluxes
        # with large loops etc. "objective fraction" is the pFBA optimality
        # and flux fraction is a multiplier for the added constraint.
        "objective_fraction": 1,
        # total sum of flux should be within 200% of minimum
        "flux_fraction": flux_fraction,
        # Set any fluxes lower then the tolerance to 0.
        "cutoff_tolerance": True,
    }

    valid_samples, convergence = sampling.sample(
        model, sampling_settings, cache_directory
    )

    # Check which reactions are significantly different and have a gene associated.
    # It is recommended to leave these parameters at the most permissive values
    # and use example_filtering for filtering
    significance_thresholds = {
        # Cutoff for distribution overlap between growth & production
        "ks_cutoff_1": 0,
        # Cutoff for distribution overlap between growth and slow growth.
        # This is usefull to filter out fluxes that are low / high
        # just because there is low growth in the production case.
        # i.e. transport reactions for minerals
        "ks_cutoff_2": 0,
        # p_cutoff will be scaled by dividing by the number of results.
        # i.e. correct for multiple testing using the Bonferroni method.
        "p_cutoff": 0.05,
        # Maximal correlation that a flux can have with the biomass.
        # Can be used to filter out even more reactions that are solely linked
        # to high or low growth and unrelated to production.
        "biomass_correlation_max": 1,
        # This is the minimal level of change in flux we choose for a reaction
        # that is feasible for intervention.
        # One could argue that this should be lower, since some reactions might have
        # extremely low fluxes even when fully utilized, such as the production of
        # a large cofactor complex. On the other hand, these reactions are likely
        # not good targets anyway.
        "abs_change_cutoff": 1e-3,
    }

    # Settings for clustering reactions into groups based on flux.
    clustering_settings = {
        # Condition that is analyzed.
        "condition": "producing",
        # CLustering method and metric (see scipy.cluster.hierarchy for details).
        "method": "average",
        "metric": "euclidean",
        "criterion": "distance",
        "cut_threshhold": 0.25,
    }

    (
        results_per_reaction,
        results_per_gene,
        gene_order,
        correlation,
    ) = sampling.summarize_results(
        valid_samples,
        model,
        target_export.id,
        biomass_reaction_id,
        essential,
        None,  # Set to None if the model already has gene names annotated properly.
        significance_thresholds,
        clustering_settings,
    )

    # Example on how group sort on correlated groups instead.
    results_per_group, group_order = sampling.sort_results(
        results_per_reaction.reset_index(),
        by="correlation_group",
        column="mean_fold_change",
    )

    # Columns to skip in output
    skip_output_columns = {}
    column_mapper = {
        "mean (condition_1)": "growing (mean)",
        "sd (condition_1)": "growing (sd)",
        "mean (condition_3)": "producing (mean)",
        "sd (condition_3)": "producing (sd)",
    }

    # ----------------------------------------------------------------------
    # Save results
    # ----------------------------------------------------------------------
    merge = True  # Merge cells in gene columns in excel sheet output.
    output, output_frame, output_order = "genes", results_per_gene, gene_order
    with pd.ExcelWriter(results_directory / "sampling_results.xlsx") as excelwriter:
        # Remove/rename output columns
        output_frame = output_frame.loc[
            :, output_frame.columns.difference(skip_output_columns)
        ].rename(columns=column_mapper)

        # Knock-down/out targets.
        if (output_order < 1).any():
            output_frame.loc[output_order.index[output_order < 1]].to_excel(
                excelwriter,
                sheet_name="{} - knock-down".format(output),
                merge_cells=merge,
            )
        # For overexpression, reverse the order.
        if (output_order > 1).any():
            output_frame.loc[output_order.index[output_order > 1][::-1]].to_excel(
                excelwriter,
                sheet_name="{} - over-express".format(output),
                merge_cells=merge,
            )
        # Sort to get unassociated at the bottom
        output_frame.loc[
            output_order.index[output_order.isna()],
        ].sort_index().to_excel(
            excelwriter,
            sheet_name="{} - insignificant".format(output),
            merge_cells=merge,
        )
        # Tab with all genes
        output_frame.to_excel(
            excelwriter, sheet_name="{} - all".format(output), merge_cells=merge
        )

        # Complete info per reaction as well
        results_per_reaction.to_excel(excelwriter, sheet_name="reactions - all")
