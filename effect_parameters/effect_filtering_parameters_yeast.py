import pathlib

import time
import csv

import cobra
import pandas as pd

import sys
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, '..')
from sampling_tools import sampling, util

if __name__ == "__main__":
    # ----------------------------------------------------------------------
    # General settings
    # ----------------------------------------------------------------------
    model_directory = pathlib.Path("../example_data")
    cache_directory = pathlib.Path("../cache/yeast")

    model_path = pathlib.Path(model_directory / "Yeast8_NAR.xml").resolve()

    target_id = "nar_c"
    biomass_reaction_id = "r_4041"
    substrate_id = "s_0565[e]" #"s_0565__91__e__93__"
    substrate_exchange_id = "r_1714"
    maintenance_reaction_id = "r_4046"
    # Change the solver to "glpk" if you don't have a gurobi license.
    solver = "glpk" #"gurobi"

    # ----------------------------------------------------------------------
    # Loading model & medium.
    # ----------------------------------------------------------------------
    if cache_directory:
        cache_directory.mkdir(exist_ok=True, parents=True)

    model = cobra.io.read_sbml_model(str(model_path))
    biomass_reaction = model.reactions.get_by_id(biomass_reaction_id)
    maintenance_reaction = model.reactions.get_by_id(maintenance_reaction_id)
    substrate = model.metabolites.get_by_id(substrate_id)
    substrate_exchange = model.reactions.get_by_id(substrate_exchange_id)

    model.solver = solver
    # max_tolerance = model.tolerance

    # Medium constraints
    inf = float("inf")
    # Substrate
    model.reactions.get_by_id(substrate_exchange_id).bounds = (-10, inf)
    # Oxygen
    model.reactions.get_by_id("r_1992").bounds = (-inf, inf)

    # Set the target export reaction.
    target_export = model.reactions.get_by_id("EX_NAR")

    # Constrain all reactions bounds to a maximum (1000).
    # No need to have truly infinite bounds when sampling.
    max_bound = sampling.COBRA_SOFT_INFINITY
    util.limit_bounds(model, max_bound)

    # Categorize reactions into different groups, which can be used to filter them out
    # from analysis. Because it takes a while, we save it to a cache.
    reaction_sets = util.get_cached_reaction_sets(
        model, maintenance_reaction_id, biomass_reaction_id, cache_directory
    )

    # Explanation:
    #   required = ATPM + Biomass
    #   not biological = boundary / exchange / sinks / demands
    #   blocked = unable to carry flux
    #   no_genes = no annotated gene
    #   essential = essential reaction
    #   essential_by_gene = reaction that have an essential gene associated
    #                       but are not necessarily essential
    #   transport = reactions in multiple compartments
    all_reactions, required, not_biological, blocked = reaction_sets[:4]
    no_genes, essential, essential_by_gene, transport = reaction_sets[4:]
    ignore = required | not_biological | blocked | no_genes | transport | essential

    # ----------------------------------------------------------------------
    # Filtering parameters to check
    # ----------------------------------------------------------------------
    ks1_list = [0] # Cutoff for distribution overlap between growth & production
    ks2_list = [0] # Cutoff for distribution overlap between growth & slow growth
    biomass_correlation_list = [1] # Maximal correlation that a flux can have with the biomass
    abs_change_list = [1e-3] # Minimal level of change in flux we choose for a reaction that is feasible for intervention.
    clustering_settings_list = [0.25] # Settings for clustering reactions into groups based on flux
   
    # ----------------------------------------------------------------------
    # Loop through sampling parameters results
    # ----------------------------------------------------------------------
    with open('runtimes_filtering_yeast_simplified.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow([
            'optimality', 'flux_fraction',
            'ks1', 'ks2', 'biomass_correlation', 'abs_change', 'clustering_settings', 'runtime', 
            'number_KD_genes', 'KD_genes', 'number_KDgenes_correlationgroups',
            'number_KO_genes', 'KO_genes', 'number_KOgenes_correlationgroups',
            'number_OV_genes', 'OV_genes', 'number_OVgenes_correlationgroups',
            'number_KD_reactions', 'KD_reactions', 'number_KDreactions_correlationgroups',
            'number_KO_reactions', 'KO_reactions', 'number_KOreactions_correlationgroups',
            'number_OV_reactions', 'OV_reactions', 'number_OVreactions_correlationgroups',
            ])  

        opt_namelist = [50, 75, 90, 95, 99]
        ff_namelist = [0, 10, 25, 50]
        for o_name in opt_namelist:
            for ff_name in ff_namelist:
                samples_path = next(pathlib.Path("../results/yeast").joinpath(f"parameters_o{o_name}_ff{ff_name}").glob("*_samples.h5"))
                valid_samples = pd.read_hdf(samples_path)

                for ks1 in ks1_list:
                    for ks2 in ks2_list:
                        for biomass_corr, biomass_corr_name in zip(biomass_correlation_list, [5, 75, 90, 100]):
                            for abs_change in abs_change_list:
                                for cluster_setting in clustering_settings_list:

                                    print(f'opt={o_name}, ff={ff_name}, ks1={ks1}, ks2={ks2}, biomass_corr={biomass_corr}, abs_change={abs_change}, clust_set={cluster_setting}')
                                    
                                    start_time = time.time()
                                    
                                    # Settings for significance and correlation
                                    significance_thresholds = {
                                        "ks_cutoff_1": ks1,
                                        "ks_cutoff_2": ks2,
                                        "p_cutoff": 0.05,
                                        "biomass_correlation_max": biomass_corr,
                                        "abs_change_cutoff": abs_change,
                                    }
                                    # Settings for clustering reactions into groups based on flux.
                                    clustering_settings = {
                                        "condition": "producing",
                                        "method": "average",
                                        "metric": "euclidean",
                                        "criterion": "distance",
                                        "cut_threshhold": cluster_setting,
                                    }
                                    # Filtering of reactions
                                    (
                                        results_per_reaction,
                                        results_per_gene,
                                        gene_order,
                                        correlation,
                                    ) = sampling.summarize_results(
                                        valid_samples,
                                        model,
                                        target_export.id,
                                        biomass_reaction_id,
                                        essential,
                                        None,
                                        significance_thresholds,
                                        clustering_settings,
                                    )

                                    # ----------------------------------------------------------------------
                                    # Record runtime and performance
                                    # ----------------------------------------------------------------------
                                    end_time = time.time()
                                    runtime = end_time - start_time
                                    # Reactions
                                    df = results_per_reaction
                                    KD_n_reactions = len(df[df['intervention']=='knock-down'])
                                    KO_n_reactions = len(df[df['intervention']=='knock-out'])
                                    OV_n_reactions = len(df[df['intervention']=='over-expression'])
                                    KD_reactions = list(df[df['intervention']=='knock-down'].index)
                                    KO_reactions = list(df[df['intervention']=='knock-out'].index)
                                    OV_reactions = list(df[df['intervention']=='over-expression'].index)
                                    KD_cg_reactions = len(set(df[df['intervention']=='knock-down']['correlation_group']))
                                    KO_cg_reactions = len(set(df[df['intervention']=='knock-out']['correlation_group']))
                                    OV_cg_reactions = len(set(df[df['intervention']=='over-expression']['correlation_group']))
                                    # Genes
                                    df = results_per_gene
                                    KD_n_genes = len(df[df['intervention']=='knock-down'])
                                    KO_n_genes= len(df[df['intervention']=='knock-out'])
                                    OV_n_genes = len(df[df['intervention']=='over-expression'])
                                    KD_genes = list(df[df['intervention']=='knock-down'].index)
                                    KO_genes = list(df[df['intervention']=='knock-out'].index)
                                    OV_genes = list(df[df['intervention']=='over-expression'].index)
                                    KD_cg_genes = len(set(df[df['intervention']=='knock-down']['correlation_group']))
                                    KO_cg_genes = len(set(df[df['intervention']=='knock-out']['correlation_group']))
                                    OV_cg_genes = len(set(df[df['intervention']=='over-expression']['correlation_group']))

                                    csv_writer.writerow([
                                        o_name, ff_name,
                                        ks1, ks2, biomass_corr, abs_change, cluster_setting, runtime, 
                                        KD_n_genes, KD_genes, KD_cg_genes, KO_n_genes, KO_genes, KO_cg_genes, OV_n_genes, OV_genes, OV_cg_genes,
                                        KD_n_reactions, KD_reactions, KD_cg_reactions, KO_n_reactions, KO_reactions, KO_cg_reactions, OV_n_reactions, OV_reactions, OV_cg_reactions,
                                        ])  

                                   
            
            
            