import json
import pathlib
import re
import time
import zipfile

import cobra
import pandas as pd
import requests


def parse_kegg_flat_file(text):
    record = {}
    current = ""
    for line in text.splitlines():
        # Kegg uses 12 characters to seperate things.
        # Parts of the data are usually seperated by multiple spaces
        term, entry = line[:12].strip(), line[12:].strip()

        if "  " in entry:
            entry = tuple(re.split("\s\s+", entry))

        # Signals end of entry
        if term == "///":
            break

        # Check if there is a new term
        if term:
            current = term

        # Add the entry to the record under term, creating a list if the term already exists.
        if current in record:
            if isinstance(record[current], list):
                record[current].append(entry)
            else:
                record[current] = [record[current], entry]
        else:
            record[current] = entry
    return record


def get_from_kegg(kegg_id, session=None, cache=None, rate_limit=None, verbose=False):
    if rate_limit:
        t0 = time.time()

    if session is None:
        session = requests.Session()

    # Check cache
    if cache is not None:
        filename = "{}.json".format(kegg_id)
        path = cache / filename

        if cache.is_file() and cache.suffix == ".zip":
            with zipfile.ZipFile(cache, "r") as zip:
                if filename in zip.namelist():
                    with zip.open(filename, "r") as file:
                        if verbose:
                            print("Cache hit: {}".format(kegg_id))
                        return json.loads(file.read().decode("utf-8"))
        else:
            if path.exists():
                with path.open("r") as file:
                    if verbose:
                        print("Cache hit: {}".format(kegg_id))
                    return json.load(file)

    # Get entry
    base_url = "http://rest.kegg.jp"
    operation = "get"
    response = session.get("/".join((base_url, operation, kegg_id)))
    # response.raise_for_status()
    if response.ok:
        if verbose:
            print("Retrieved from Kegg: {}".format(kegg_id))
        record = parse_kegg_flat_file(response.text)
    elif response.status_code == 404:
        if verbose:
            print("Invalid identifier: {}".format(kegg_id))
        record = "Invalid identifier"
    else:
        response.raise_for_status()

    # Save to cache
    if cache is not None:
        if cache.suffix == ".zip":
            with zipfile.ZipFile(cache, "a", zipfile.ZIP_DEFLATED) as zip:
                with zip.open(filename, "w") as file:
                    file.write(json.dumps(record, indent=2).encode("utf-8"))
        else:
            with path.open("w") as file:
                json.dump(record, file, indent=2)

    # If rate_limit is set, wait the required time before returning.
    if rate_limit:
        elapsed = time.time() - t0
        wait = rate_limit - elapsed
        if wait > 0:
            time.sleep(wait)

    return record


def get_kegg_identifiers(model):
    return (
        {met.id: met.annotation.get("kegg.compound") for met in model.metabolites},
        {reac.id: reac.annotation.get("kegg.reaction") for reac in model.reactions},
    )


def get_pathway_information(model, cache_location=None, rate_limit=0.25, verbose=False):
    metabolites, reactions = get_kegg_identifiers(model)
    m_records, r_records = {}, {}
    with requests.Session() as s:
        for mid, kegg_id in metabolites.items():
            if kegg_id:
                m_records[mid] = get_from_kegg(
                    kegg_id, s, cache_location, rate_limit, verbose
                )
            else:
                m_records[mid] = None

        for rid, kegg_id in reactions.items():
            if kegg_id:
                r_records[rid] = get_from_kegg(
                    kegg_id, s, cache_location, rate_limit, verbose
                )
            else:
                r_records[mid] = None

    def flatten(kegg_data, key):
        if key in kegg_data:
            if isinstance(kegg_data[key], (list, tuple)) and not isinstance(
                kegg_data[key][0], (list, tuple)
            ):
                data = [kegg_data[key]]
            else:
                data = kegg_data[key]
        else:
            data = []
        yield from data

    entries = []
    for metabolite, kegg_data in m_records.items():
        if kegg_data is None or kegg_data == "Invalid identifier":
            continue
        entry = (
            metabolite,
            model.metabolites.get_by_id(metabolite).name,
            kegg_data["ENTRY"][0],
            "Metabolite",
        )
        for id, name in flatten(kegg_data, "PATHWAY"):
            entries.append(entry + ("pathway_map", id, name))
        for id, name in flatten(kegg_data, "MODULE"):
            entries.append(entry + ("module", id, name))

    for reaction, kegg_data in r_records.items():
        if kegg_data is None or kegg_data == "Invalid identifier":
            continue
        entry = (
            reaction,
            model.reactions.get_by_id(reaction).name,
            kegg_data["ENTRY"][0],
            "Reaction",
        )
        for id, name in flatten(kegg_data, "PATHWAY"):
            entries.append(entry + ("pathway", id, name))
        for id, name in flatten(kegg_data, "MODULE"):
            entries.append(entry + ("module", id, name))

    df = pd.DataFrame(
        entries,
        columns=[
            "model_id",
            "model_name",
            "kegg_id",
            "entry_type",
            "group_type",
            "group_id",
            "group_name",
        ],
    )
    return df


if __name__ == "__main__":
    cache_location = pathlib.Path("cache/kegg/cache.zip")
    if cache_location.suffix != ".zip":
        cache_location.mkdir(exist_ok=True, parents=True)
    else:
        cache_location.parent.mkdir(exist_ok=True, parents=True)

    entries = ["R00197", "R09376", "C00965", "C11514"]
    records = {}
    with requests.Session() as s:
        for entry in entries:
            records[entry] = get_from_kegg(entry, s, cache_location, 0.5)

    model = cobra.io.read_sbml_model("example_data/iJN1462_fbc.xml")
    df = get_pathway_information(model, cache_location, 0.25)
