import pathlib

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from sampling_tools import sampling, util


# Load sampling results
cache_path = next(pathlib.Path("../cache/yeast").glob("*_samples.h5"))
samples = pd.read_hdf(cache_path) 


# Example plots
cache_path = next(pathlib.Path("../cache/yeast").glob("*_samples.h5"))
samples = pd.read_hdf(cache_path) #pd.read_hdf("cache/yeast")

for r in ['r_0109', 'r_1050', 'r_0471', 'r_0503', 'r_0961', 'r_2093', 'r_0938']:
  fig, ax = util.plot_distributions(samples[[r, "condition"]], quantile=0.9)
  fig.savefig("plots/{}.png".format(r), dpi=300)
